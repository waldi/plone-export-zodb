import logging
import sys
import ConfigParser


class ScriptBase(object):
    def __init__(self, config, debug=False):
        logging.basicConfig(level=debug and logging.DEBUG or logging.INFO, stream=sys.stdout)

        self.config = ConfigParser.SafeConfigParser()
        self.config.read(config)

