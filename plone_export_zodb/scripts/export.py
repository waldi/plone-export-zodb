import argparse
import codecs
import collections
import json
import logging
import os

from . import ScriptBase


class Script(ScriptBase):
    def __call__(self):
        from ..zope.config import create_zope_db_item_filter
        zope_db, zope_item = create_zope_db_item_filter(self.config, 'input')

        try:
            work = collections.deque()

            work.append(zope_item)

            while work:
                in_item = work.popleft()
                logging.debug('Work on %s', in_item.url)

                for i, ip in in_item.items():
                    work.append(ip)

                self.export(in_item)

        finally:
            zope_db.close()

    def export(self, item):
        path = os.path.join('output', item.url)
        parent_path = os.path.dirname(path)

        try:
            os.makedirs(parent_path)
        except:
            pass

        with open(path + '.json', 'w') as f:
            json.dump({
                'title': item.title,
                'navtitle': item.navtitle,
                'description': item.description,
                'text': item.text,
                'type': item.type,
            }, f, indent=2, sort_keys=True)

    @classmethod
    def run(cls):
        parser = argparse.ArgumentParser(description='Export Plone.')
        parser.add_argument('--debug', dest='debug', action='store_true')
        parser.add_argument('config')
        args = parser.parse_args()

        cls(**args.__dict__)()


if __name__ == '__main__':
    Script.run()
