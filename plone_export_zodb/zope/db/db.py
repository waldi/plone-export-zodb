import logging
import collections
import os
import stat
import cPickle

from itertools import izip
from cStringIO import StringIO

from ZODB.blob import BlobStorage
from ZODB.FileStorage import FileStorage


logger = logging.getLogger(__name__)
_type_cache = {}


class BaseObject(object):
    __slots__ = 'newargs', 'state'

    def __new__(cls, *args):
        result = object.__new__(cls)
        result.newargs = args
        return result

    def __setstate__(self, state):
        self.state = state


class DbObject(BaseObject):
    __real__ = 'unknown'
    __slots__ = 'db', 'oid', 'serial', 'size', 'refs'

    @classmethod
    def type_find(cls, module, name):
        complete_name = module + '.' + name
        work = collections.deque((cls, ))
        while work:
            cls = work.popleft()
            if complete_name == cls.__real__:
                return cls
            work.extend(cls.__subclasses__())

    @classmethod
    def type_generate(cls, module, name):
        global _type_cache
        cache = _type_cache.setdefault((cls.__module__, cls.__name__), {})
        c = cache.get((module, name))
        if c:
            return c
        complete_name = module + '.' + name
        c_name = cls.__name__ + '.__real__.' + complete_name
        c = type(c_name, (cls, ), {'__real__': complete_name})
        cache[module, name] = c
        return c

    @classmethod
    def load(cls, db, oid, version=''):
        pickle, serial = db.fs.load(oid, version)

        info, data, proto_refs = cls._unpickle(pickle)

        if isinstance(info, tuple):
            if isinstance(info[0], type):
                ret = info[0].__new__(*info)
            else:
                ret = DbObject.__new__(DbObject, *info)
        else:
            ret = info.__new__(info)

        refs = set()
        for ref in proto_refs:
            if isinstance(ref, tuple):
                ref = ref[0]
            refs.add(ref)

        ret.__load__(db, oid, serial, pickle, data, frozenset(refs))

        return ret

    @classmethod
    def _unpickle(cls, pickle):
        refs = []
        u = cPickle.Unpickler(StringIO(pickle))
        u.find_global = cls._unpickle_find_global
        u.persistent_load = refs

        info = u.load()
        data = u.load()

        return info, data, refs

    @classmethod
    def _unpickle_find_global(cls, module, name):
        return (cls.type_find(module, name) or
                DbStandardObject.type_find(module, name) or
                cls.type_generate(module, name))

    def __load__(self, db, oid, serial, pickle, data, refs):
        self.db = db
        self.oid = oid
        self.serial = serial
        self.size = len(pickle)
        self.refs = refs

    def __repr__(self):
        return '<db %s object>' % self.__real__

    @property
    def blob_size(self):
        return None


class DbStandardObject(DbObject):
    __slots__ = ()


class DbBlob(DbStandardObject):
    __real__ = 'ZODB.blob.Blob'
    __slots__ = ()

    @property
    def blob_filename(self):
        return self.db.blob.loadBlob(self.oid, self.serial)

    @property
    def blob_size(self):
        return os.stat(self.blob_filename)[stat.ST_SIZE]


class DbPersistentListObject(DbStandardObject, collections.Sequence):
    __real__ = 'persistent.list.PersistentList'
    __slots__ = '_data'

    def __load__(self, db, oid, serial, pickle, data, refs):
        super(DbPersistentListObject, self).__load__(db, oid, serial, pickle, data, refs)
        self._data = data['data']

    def __iter__(self):
        return iter(self._data)

    def __len__(self):
        return len(self._data)

    def __getitem__(self, key):
        return self._data[key]


class DbPersistentMappingObject(DbStandardObject, collections.Mapping):
    __real__ = 'Persistence.mapping.PersistentMapping'
    __slots__ = '_data'

    def __load__(self, db, oid, serial, pickle, data, refs):
        super(DbPersistentMappingObject, self).__load__(db, oid, serial, pickle, data, refs)
        self._data = data['data']

    def __iter__(self):
        return iter(self._data)

    def __len__(self):
        return len(self._data)

    def __getitem__(self, key):
        return self._data[key]


class DbOOBTreeObject(DbStandardObject, collections.Mapping):
    __real__ = 'BTrees.OOBTree.OOBTree'
    __slots__ = '_items', '_items_refs'

    def __load__(self, db, oid, serial, pickle, data, refs):
        super(DbOOBTreeObject, self).__load__(db, oid, serial, pickle, data, refs)
        if not data or not data[0]:
            self._items = {}
            return
        b = data[0]
        if len(b) == 1:
            self._items = dict(izip(b[0][0][0::2], b[0][0][1::2]))
        else:
            self._items = None
            self._items_refs = tuple(i[0] for i in b[0::2])

    def _items_init(self):
        if self._items is None:
            self._items = {}
            for ref in self._items_refs:
                i = DbObject.load(self.db, ref)
                self._items.update(izip(i._data[0::2], i._data[1::2]))

    def __iter__(self):
        self._items_init()
        return iter(self._items)

    def __len__(self):
        self._items_init()
        return len(self._items)

    def __getitem__(self, key):
        self._items_init()
        return self._items[key]

    def iteritems(self):
        self._items_init()
        return self._items.iteritems()


class Db_OOBTreeObject(DbOOBTreeObject):
    __real__ = 'BTrees._OOBTree.OOBTree'
    __slots__ = ()


class DbOOBBucketObject(DbObject):
    __real__ = 'BTrees.OOBTree.OOBucket'
    __slots__ = '_data', '_next'

    def __load__(self, db, oid, serial, pickle, data, refs):
        self._data = data[0]
        if len(data) > 1:
            self._next = data[1]


class Database(object):
    def get(self, oid, version='', cls=DbStandardObject):
        return cls.load(self, oid, version)

    def get_ref(self, ref, cls=DbStandardObject):
        if isinstance(ref, str):
            oid = ref
        elif isinstance(ref, tuple):
            oid = ref[0]
        elif ref is None:
            return
        else:
            raise NotImplementedError
        return self.get(oid, cls=cls)

    def __init__(self, fs, blob_dir=None):
        self.fs = FileStorage(fs, read_only=1)
        self.blob = blob_dir and BlobStorage(blob_dir, self.fs)

    def close(self):
        self.fs.close()
