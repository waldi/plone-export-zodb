import logging

from .db import DbObject, DbPersistentMappingObject
from .tree import TreeObject


logger = logging.getLogger(__name__)


class ItemObject(TreeObject):
    __slots__ = 'title', 'permissions', 'roles', 'workflow_history', 'metadata'

    def __load__(self, db, oid, serial, pickle, data, refs):
        super(ItemObject, self).__load__(db, oid, serial, pickle, data, refs)
        self.title = data.get('title')
        if isinstance(self.title, str):
            self.title = self.title.decode('utf-8')
        workflow_history = None
        workflow_history_oid = data.get('workflow_history')
        if workflow_history_oid:
            workflow_history = db.get_ref(workflow_history_oid, cls=DbPersistentMappingObject)
        self.workflow_history = workflow_history

        self.permissions = perms = {}
        for key, value in data.iteritems():
            if key.startswith('_') and key.endswith('_Permission') and isinstance(value, (list, tuple)):
                perms[key[1:-11]] = set(value), isinstance(value, list)

        self.roles = roles = {}
        for key, value in (data.get('__ac_local_roles__') or {}).iteritems():
            roles[key] = set(value)

        self.metadata = db.get_ref(data.get('_md'), cls=DbPersistentMappingObject)


class DocumentObject(ItemObject):
    __slots__ = 'text'
    __real__ = 'Products.ATContentTypes.content.document.ATDocument'
    __type__ = 'document'

    def __load__(self, db, oid, serial, pickle, data, refs):
        super(DocumentObject, self).__load__(db, oid, serial, pickle, data, refs)
        an = db.get_ref(data['__annotations__'])
        self.text = db.get_ref(an['Archetypes.storage.AnnotationStorage-text'], cls=BaseUnitObject)


class FolderObject(ItemObject):
    __slots__ = 'default_page', 'container_order'
    __type__ = 'folder'

    def __load__(self, db, oid, serial, pickle, data, refs):
        super(FolderObject, self).__load__(db, oid, serial, pickle, data, refs)
        self.default_page = data.get('default_page')


class LinkObject(ItemObject):
    __slots__ = 'url'
    __real__ = 'Products.ATContentTypes.content.link.ATLink'
    __type__ = 'link'

    def __load__(self, db, oid, serial, pickle, data, refs):
        super(LinkObject, self).__load__(db, oid, serial, pickle, data, refs)
        self.url = data['remoteUrl']


class ATFolderObject(FolderObject):
    __slots__ = ()
    __real__ = 'Products.ATContentTypes.content.folder.ATFolder'

    def __load__(self, db, oid, serial, pickle, data, refs):
        super(ATFolderObject, self).__load__(db, oid, serial, pickle, data, refs)
        an_ref = data.get('__annotations__')
        an = an_ref and db.get_ref(an_ref)
        p = an and an.get('plone.folder.ordered.order')
        self.container_order = p and list(db.get_ref(p))


class PloneSiteObject(FolderObject):
    __slots__ = ()
    __real__ = 'Products.CMFPlone.Portal.PloneSite'

    def __load__(self, db, oid, serial, pickle, data, refs):
        super(PloneSiteObject, self).__load__(db, oid, serial, pickle, data, refs)
        objects = data['_objects']
        self.container_order = [i['id'] for i in objects]


class BlobObject(ItemObject):
    __slots__ = 'content_type', 'filename', 'images', '__type__'
    __real__ = 'plone.app.blob.content.ATBlob'

    class _BlobScale(object):
        __slots__ = 'content_type', 'filename'

        def __init__(self, content_type, filename):
            self.content_type, self.filename = content_type, filename

    def __load__(self, db, oid, serial, pickle, data, refs):
        super(BlobObject, self).__load__(db, oid, serial, pickle, data, refs)
        an = db.get_ref(data['__annotations__'])

        if data['portal_type'] == 'File':
            wrap = db.get_ref(an['Archetypes.storage.AnnotationStorage-file'], cls=BlobWrapperObject)
            self.__type__ = 'file'
            self.images = {}
        elif data['portal_type'] == 'Image':
            wrap = db.get_ref(an['Archetypes.storage.AnnotationStorage-image'], cls=BlobWrapperObject)
            self.__type__ = 'image'
            self.images = {}
            scales = data.get('__blob_scales')
            if scales:
                for scale in data['__blob_scales']['image'].itervalues():
                    blob = db.get_ref(scale['blob'], cls=BlobWrapperObject)
                    self.images[scale['id']] = self._BlobScale(scale['content_type'], blob.blob_filename)
        else:
            raise NotImplementedError

        self.content_type = wrap.content_type
        try:
            self.filename = wrap.blob.blob_filename
        except:
            self.filename = None


class BaseUnitObject(DbObject):
    __slots__ = 'mimetype', 'raw'
    __real__ = 'Products.Archetypes.BaseUnit.BaseUnit'

    def __load__(self, db, oid, serial, pickle, data, refs):
        super(BaseUnitObject, self).__load__(db, oid, serial, pickle, data, refs)
        self.mimetype = data['mimetype']
        self.raw = data['raw']


class BlobWrapperObject(DbObject):
    __slots__ = 'blob', 'content_type'
    __real__ = 'plone.app.blob.field.BlobWrapper'

    def __load__(self, db, oid, serial, pickle, data, refs):
        super(BlobWrapperObject, self).__load__(db, oid, serial, pickle, data, refs)
        self.blob = db.get_ref(data['blob'])
        self.content_type = data['content_type']
