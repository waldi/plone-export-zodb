import logging

from .db import DbObject


logger = logging.getLogger(__name__)


class TreeObject(DbObject):
    __slots__ = 'container'

    def __load__(self, db, oid, serial, pickle, data, refs):
        container = {}
        logger.debug('Loading {!r}'.format(self))
        if isinstance(data, dict):
            if '_container' in data:
                container = data['_container']
            elif '_objects' in data:
                for i in data['_objects']:
                    i = i['id']
                    ref = data.get(i)
                    if isinstance(ref, tuple):
                        ref = ref[0]
                    container[i] = ref
            elif '_tree' in data:
                tree = DbObject.load(db, data['_tree'][0])
                for key, value in tree.iteritems():
                    container[key] = value[0]
                logger.debug('Found _tree: {}'.format(sorted(container.keys())))

        self.container = container
        refs -= frozenset(container)

        super(TreeObject, self).__load__(db, oid, serial, pickle, data, refs)

    def __getitem__(self, key):
        return self.container[key]


class TreeRootObject(DbObject):
    __slots__ = 'app'

    def __load__(self, db, oid, serial, pickle, data, refs):
        self.app = data['_container']['Application']

        super(TreeRootObject, self).__load__(db, oid, serial, pickle, data, refs)
