def create_zope_db(config, section):
    from .db.db import Database

    fs = config.get(section, 'plone_db_fs')
    blob = config.get(section, 'plone_db_blob')

    return Database(fs, blob)


def create_zope_db_item(config, section):
    from .item import Item

    db = create_zope_db(config, section)

    path = config.get(section, 'plone_db_path')

    return db, Item.find(db, path)


def create_zope_db_item_filter(config, section):
    import importlib
    from .filter import ItemFilter

    db, item = create_zope_db_item(config, section)

    filters_string = config.get(section, 'plone_filters')

    filters = []

    for filter_string in filters_string.split():
        if '(' in filter_string:
            filter_string, args = filter_string.rsplit('(', 1)
            args = eval('(' + args)
        else:
            args = ()

        module_name, filter_name = filter_string.rsplit('.', 1)
        module = importlib.import_module(module_name)
        filters.append(getattr(module, filter_name)(*args))

    return db, ItemFilter(filters, item)
