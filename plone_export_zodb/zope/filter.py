import collections
import logging


logger = logging.getLogger(__name__)


class ItemFilter(collections.Mapping):
    __slots__ = '_filters', '_inner', '__children_cache'

    def __init__(self, filters, inner):
        self._filters, self._inner = filters, inner

        self.__children_cache = None

    @property
    def __children(self):
        cache = self.__children_cache
        if cache is None:
            self.__children_cache = cache = set()
            for name, item in self._inner.iteritems():
                accept_global = None
                for filter in self._filters:
                    accept = filter.accept(item)
                    if accept in (True, False):
                        accept_global = accept
                        break

                if accept_global is True:
                    cache.add(name)

        return cache

    def __iter__(self):
        return iter(self.__children)

    def __len__(self):
        return len(self.__children)

    def __getitem__(self, key):
        return self.__class__(self._filters, self._inner[key])

    def __nonzero__(self):
        return True

    def __getattribute__(self, key):
        try:
            return super(ItemFilter, self).__getattribute__(key)
        except AttributeError:
            if key.startswith('_'):
                raise

        return getattr(self._inner, key)


class ItemFilterPermission(object):
    def __init__(self, permission, user):
        self.permission, self.user = permission, user

    def accept(self, item):
        perms = item.get_permissions().get(self.permission, ())
        if self.user in perms:
            logger.debug('{}: Accept permission'.format(item.url))
            return True

        logger.info('{}: Ignoring due to permission'.format(item.url))
        return False
