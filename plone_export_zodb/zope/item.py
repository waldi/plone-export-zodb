import collections
import logging
import traceback
import urllib
import urlparse

from ZODB.POSException import POSKeyError
from ZODB.utils import z64

from .db.item import ItemObject, DocumentObject, FolderObject, BlobObject, LinkObject, BaseUnitObject
from .db.tree import TreeObject, TreeRootObject

logger = logging.getLogger(__name__)


class _Children(collections.Mapping):
    __slots__ = 'url', 'parent', '_db', '_entry', '_entry_folder', '__children_cache'

    def __init__(self, url, parent, db, entry):
        self.url, self.parent, self._db = url, parent, db

        default_page = None
        entry_folder = None

        if isinstance(entry, FolderObject):
            entry_folder = entry
            for i in entry_folder.default_page, 'index_html', 'index.htm':
                entry_oid = entry_folder.container.get(i)
                if entry_oid:
                    logger.debug('{}: Found default page: {}'.format(url, i))
                    entry_new = db.get(entry_oid, cls=TreeObject)
                    logger.debug('{}: Found default page: {!r}'.format(url, entry_new))
                    if isinstance(entry_new, ItemObject):
                        default_page = i
                        entry = entry_new
                    break

        if not isinstance(entry, ItemObject):
            raise RuntimeError(repr(entry))

        self._entry, self._entry_folder = entry, entry_folder
        self._default_page = default_page
        self.__children_cache = None

    @property
    def __children(self):
        cache = self.__children_cache
        if cache is None:
            self.__children_cache = cache = {}
            if self._entry_folder:
                for name, child_oid in self._entry_folder.container.iteritems():
                    if name == self._default_page:
                        continue
                    try:
                        o = self._db.get(child_oid, cls=TreeObject)
                        if isinstance(o, ItemObject):
                            cache[name] = o
                        else:
                            logger.debug('%s/%s: Ignore: %r', self.url, name, o.__real__)
                    except POSKeyError:
                        pass
        return cache

    def __iter__(self):
        return iter(self.__children)

    def __len__(self):
        return len(self.__children)

    def __getitem__(self, key):
        return Item(self.url + '/' + key, self, self._db, self.__children[key])

    def items(self):
        for key, entry in self.__children.iteritems():
            yield key, Item(self.url + '/' + key, self, self._db, entry)

    def values(self):
        for k, v in self.items():
            yield v


class Item(_Children):
    __slots__ = ()

    @classmethod
    def find(cls, db, path):
        root = db.get(z64, cls=TreeRootObject)
        entry = db.get(root.app, cls=TreeObject)
        for i in path.split('/'):
            entry = db.get(entry.container[i], cls=TreeObject)
        return cls('.', None, db, entry)

    def __nonzero__(self):
        return True

    @property
    def title(self):
        title = self._entry.title
        if title:
            if not isinstance(title, unicode):
                title = unicode(title, 'utf-8')
        return title

    @property
    def navtitle(self):
        if self._entry_folder:
            navtitle = self._entry_folder.title
            if navtitle:
                if not isinstance(navtitle, unicode):
                    navtitle = unicode(navtitle, 'utf-8')
                return navtitle
        return self.title

    @property
    def description(self):
        description_ref = self._entry.metadata.get('description')
        if description_ref:
            description = self._db.get_ref(description_ref, cls=BaseUnitObject)
            raw = description.raw
            if raw:
                if isinstance(raw, str):
                    raw = raw.decode('utf-8')
                return raw

    @property
    def text(self):
        if isinstance(self._entry, DocumentObject):
            text = self._entry.text
            mimetype = text.mimetype

            logger.debug('{}: Got text: {!r}'.format(self.url, text))
            if text:
                error = None

                raw = text.raw
                if isinstance(raw, str):
                    raw = raw.decode('utf-8')

                return raw
        else:
            logger.debug('{}: Got wrong type for .text: {!r}'.format(self.url, self._entry))

    @property
    def order(self):
        if self._entry_folder:
            order = self._entry_folder.container_order
            if order:
                for i in self._entry_folder.default_page, 'index_html', 'index.htm':
                    if i in order:
                        del order[order.index(i)]
            return order

    @property
    def link(self):
        if isinstance(self._entry, LinkObject):
            url = urlparse.urljoin(self.url + '/', self._entry.url.rstrip('/'))
            url_split = urlparse.urlsplit(url)

            if not url_split.netloc:
                return '.' + urllib.unquote(urlparse.urljoin('/', url_split.path))

    @property
    def content_type(self):
        return self._entry.content_type

    def get_exclude_from_nav(self):
        entry = self._entry_folder or self._entry
        return entry.metadata and entry.metadata.get('excludeFromNav')

    def get_filename(self):
        return self._entry.filename

    def get_images(self):
        return self._entry.images

    def get_permissions(self):
        ret = {}

        permissions = {}
        roles = {}

        cur = self
        while cur is not None:
            entry = cur._entry_folder or cur._entry
            cur = cur.parent

            for perm, (perm_roles, perm_inherit) in entry.permissions.iteritems():
                p = permissions.setdefault(perm, [set(), True])
                if p[1]:
                    p[0].update(perm_roles)
                    p[1] = perm_inherit

            for role, role_users in entry.roles.iteritems():
                roles.setdefault(role, set()).update(role_users)

        for perm, (perm_roles, perm_inherit) in permissions.iteritems():
            ret[perm] = r = set(perm_inherit and (u'Anonymous', ) or ())
            for user, user_roles in roles.iteritems():
                if user_roles & perm_roles:
                    r.add(user)

        return ret

    @property
    def type(self):
        return self._entry.__type__
